# 📚 Git Learning - Init & Clone

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

In this course, you will learn how to create a new local repository from nothing or clone a remote one from Gitlab.

## 🏁 Init a new repository

At **Git School**, you do not need to create your own repo from scratch since we always provide you with one already existing, but we are convinced that one day, you will need this command.

To create a new repository in your current directory, you need only to enter one command.

```bash
git init
```

Alone, this command only initializes the local repository. You will learn later how to add new files, commit them and push them to a remote repository.

## ✒️ Clone an existing repository

Cloning an existing repository allows you to download the whole history of a remote repo onto your computer and *checkout* the latest version on the *default branch*. We'll cover these concepts later, but for now just imagine that you have the whole history on your computer and that your files correspond to the most recent version.

*This is overly simplified and wrong at some extent but the **Git School** will rectify the explanation later.*

In order to clone, you need a url. You can usually find it by clicking on the `Clone` button found on the top right of the project page. There is a SSH and HTTPS link. We'll use the HTTPS (`https://gitlab.com/git-course/01-init-and-clone.git`) one and learn how to setup the environment to accept the SSH one in future courses.

Cloning a project will always automatically create a new directory to hold the cloned contents. If you are not happy with that, you may specify the name yourself.

```bash
git clone <url> [directory]
```

*For the sake of these courses, an argument written between `<>` is mandatory and between `[]` is optional.*

## 🧑‍🎓 Exercise

Now is your turn to practice. Today's exercise is simply to clone this repository on your computer.

Every course will have its own exercises and a file named `check.sh` that will automatically verify that everything is correct. This file assumes that `/bin/bash` is installed on your computer. You can run it with `./check.sh` or with your preferred shell `zsh check.sh`.

Once everything is green, you may proceed to the [next course](https://gitlab.com/git-course/02-git-add).