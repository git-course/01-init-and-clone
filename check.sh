#!/bin/bash

# Set working directory to the script location in case the user
# did not run the script in the project root.
cd "${0%/*}"

# Retrieve first commit in history
FIRST_COMMIT="$(git rev-list --max-parents=0 HEAD)"

# Correct 
EXPECTED_COMMIT='db10be403c14ddc3c92157505d70040dc35772b2'

# Colors
GREEN="\033[1;32m"
RED="\033[0;31m"
NC="\033[0m"

if [ "$FIRST_COMMIT" = "$EXPECTED_COMMIT" ]; 
then 
    echo -e "$GREEN✔$NC"" Clone successful!"; 
else
    echo -e "$RED✘$NC"" Clone failed. Have you used the git clone command?";
fi;