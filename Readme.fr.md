# 📚 Git Learning - Init & Clone

This Readme is available in: [English](./Readme.md), [Français](./Readme.fr.md).

Dans ce cours, tu vas apprendre comment créer un nouveau dépôt local depuis rien ou à en copier (*clone*) un depuis Gitlab.

## 🏁 Initialiser un nouveau dépôt

Dans l'**École de Git**, il n'est pas nécessaire de créer ton propre dépôt depuis rien puisque nous en fournissons toujours un. Mais à l'avenir, tu auras peut-être besoin de connaître cette commande.

Pour créer un nouveau dépôt dans ton répertoire courant, tu as besoin d'une seule commande.

```bash
git init
```

Seule, cette commande initialise uniquement le dépôt local. Tu apprendras plus tard comment ajouter des fichiers, créer un commit et les *push* sur un dépôt distant.

## ✒️ Cloner un dépôt

Cloner un dépôt existant te permet de télécharger l'entièreté de l'historique d'un dépôt distant sur ton ordinateur et de faire un *checkout* sur la dernière version de la *branche par défaut*. Nous parlerons plus tard de ces nouveaux concepts, mais pour l'instant tu peux supposer que tu as l'historique complet sur ton ordinateur et que tes fichiers correspondent à la version la plus récente.

*Cette explication est grandement simplifiée et partiellement fausse mais l'**École de Git** reviendra dessus plus tard.*

Pour cloner, tu as besoin d'une URL. Tu peux la trouver en général en cliquant sur le bouton `Clone` qui se trouve en haut à droite de la page de projet. Tu devrais y voir un lien SSH et un lien HTTPS. Nous utiliserons HTTPS (`https://gitlab.com/git-course/01-init-and-clone.git`) et apprendrons à mettre en place ton environnement pour utiliser le lien SSH dans un cours ultérieur.

Cloner un projet va toujours automatiquement créer un nouveau dossier pour contenir les fichiers. Si ce fonctionnement n'est pas satisfaisant, il est possible de définir nous-même le nom.

```bash
git clone <url> [directory]
```

*Dans le cadre de ces cours, un argument écrit entre `<>` est obligatoire alors qu'il est facultatif s'il est entre `[]`.*

## 🧑‍🎓 Exercice

Laissons place à la pratique. Pour ce cours, tu dois simplement réussir à cloner ce repo sur ton ordinateur.

Chaque cours aura ses propres exercices et un fichier nommé `check.sh` qui vérifiera automatiquement si tout est correct (en anglais). Ce fichier suppose que `/bin/bash` est installé sur ton ordinateur. Tu peux le lancer avec `./check.sh` ou ton shell préféré `zsh check.sh`.

Une fois que tout est vert, tu peux charger le [cours suivant](https://gitlab.com/git-course/02-git-add).